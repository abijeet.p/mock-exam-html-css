# Mock exam

## Please read the instructions carefully

1. Comments are in a yellow sticky paper.
2. Please note the colors carefully. Match the font and background colors to the best of your ability.
3. Font sizes are also important. Match the image as much as possible.
4. Determine heading levels (h1, h2, h3, h4) based on the content and its location.
5. Please ignore responsiveness for now. Makes sure the page looks good at a width of 1200px or more.

Best of luck!

---

# Screen 1

![Mockups](https://gitlab.com/abijeet.p/mock-exam-html-css/raw/master/batch9_mockup.png)

# Screen 2

You will create another page, where interested people can apply to attend events conducted by the BlackPanther group. The form will have the following fields,

1. Full Name 
2. Email address
3. Company
4. Gender
5. Date of birth
6. Reason for Attending
7. ID Proof (Passport / Driver License) etc

The header and footer will be copied from the previous page.